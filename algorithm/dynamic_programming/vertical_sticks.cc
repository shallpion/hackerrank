#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <unordered_map>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
	int T, N;
	cin >> T;
	while(T-- > 0) {
		cin >> N;
		vector<int> stks(N);
		for(int i = 0; i < N; i++) {
			cin >> stks[i] ;
		}
		sort(stks.begin(), stks.end(), greater<int>());

		unordered_map<int, int> order;
		for(int i = 0; i < N; i++) {
			// notice that this automatically corrects the duplicate problem
			order[stks[i]]=i+2;	
		}
		double e = 0.0;
		for(int i = 0; i < N; i++) {
			e += (N+1)/(double)order[stks[i]];
		}
		cout << fixed << setprecision(2) << e << endl;

	}
    return 0;
}
