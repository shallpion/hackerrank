// brutal force, TLE
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits.h>
using namespace std;

int dist(const pair<int, int> &p1, const pair<int, int> &p2) {
	return max(abs(p1.first - p2.first), abs(p1.second - p2.second));
}

int cost(const vector<pair<int, int>> &wells) {
	int ret = 0;
	int d = 0;
	for(unsigned i = 0; i < wells.size(); i++) {
		d = 0;
		for(unsigned j = 0; j < i; j++) {
			int t =  dist(wells[i], wells[j]);
			d = max(d, t);
		}
		ret += d;
	}
	return ret;
}
void perm_traversal(vector<pair<int, int>> &wells, unsigned bnd, int &ans) {
	if(bnd == wells.size()) {
		// a full permutation, start working
		int running_cost = cost(wells);
		ans = min(ans, running_cost);
	}
	// generate the next permutation, can also use next_permutation from
	// std::algorithm
	for(unsigned i = bnd; i < wells.size(); i++) {
		swap(wells[i], wells[bnd]);
		perm_traversal(wells, bnd+1, ans);
		swap(wells[i], wells[bnd]);
	}
}
int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
	int r, c;
	cin >> r >> c;
	int a_ij;
	vector<pair<int, int>> wells;
	for(int i = 0; i < r; i++) {
		for(int j = 0; j < c; j++) {
			cin >> a_ij;
			if(a_ij) wells.push_back({i, j});
		}
	}
	int ans = INT_MAX;
	perm_traversal(wells, 0, ans);
	cout << ans;
    return 0;
}
