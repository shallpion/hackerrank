// the idea is the decrease a single employee's chocolate by one, two or five.
// the idea is the decrease a single employee's chocolate by one, two or five.
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits.h>
using namespace std;

inline int v_min(const vector<int> &v) {
	int m = v[0];
	for(int i = 0; i < (int) v.size(); ++i) {
		m = min(m, v[i]);
	}
	return m;
}

inline int desc(int low, int up) {
	if(up == low) return 0;
	int ret = 0;
	int diff = up - low;
	ret += diff/5;
	diff %= 5;
	ret += diff/2;
	diff %= 2;
	ret += diff;
	return ret;
}
int main() {
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */   
	int T, N;
	cin >> T;
	for(int i = 0; i < T; i++) {
		cin >> N;
		vector<int>clg(N);	//colleagues
		for(int j = 0; j < N; j++) {
			cin >>clg[j];
		}
		// target
		int tgt = v_min(clg);
		long cho = LONG_MAX;	// number of chocolates
		long t = 0;
		for(int k = 0; k <= 5; k++) {
			t = 0;
			for(int j = 0; j < N; j++) {
				t += desc(tgt-k, clg[j]);
			}
			cho = min(t, cho);
		}
		cout << cho << endl;

	}
	return 0;
}
