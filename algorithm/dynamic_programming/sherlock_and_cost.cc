#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
	int T, N;
	cin >>T;
	while(T-- > 0) {
		cin >> N;
		vector<int> B(N);
		for(int i = 0; i < N; i++) {
			cin >> B[i];
		}
		vector<vector<int>> dp(N, vector<int>(2, 0));
		// dp[i][0] means we choose A_i = 1
		// dp[i][1] means we choose A_i = B_i;
		for(int i = 1; i < N; i++) {
			dp[i][0] = max(dp[i][0], B[i-1]-1+dp[i-1][1]);
			dp[i][1] = max(dp[i][1], B[i]-1+dp[i-1][0]);
			dp[i][1] = max(dp[i][1], abs(B[i]-B[i-1])+dp[i-1][1]);
		}
		cout << max(dp[N-1][0], dp[N-1][1]) << endl;


	}

    return 0;
}
