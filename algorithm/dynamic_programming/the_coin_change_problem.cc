
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
	int N, size;
	cin >> N >> size;
	vector<int> coins;
	int t;
	for(int i = 0; i < size; i++) {
		cin >> t;
		coins.push_back(t);
	}
	
	vector<vector<long>> dp(size+1, vector<long>(N+1, 0));
	for(int i = 0; i < size+1; i++) {
		// no matter how many coins you have you always have one way to pay zero
		// amount
		dp[i][0] = 1;
	}


	for(int i = 1; i < size+1; i++) {
		for(int j = 1; j < N+1; j++) {
			if(j < coins[i-1]) {
				// coins[i-1] is too large for j amount
				dp[i][j] = dp[i-1][j];
			} else {
				// two cases, either we don't use coins[i-1] at all, or
				// we use it at least once,
				dp[i][j] = dp[i-1][j] + dp[i][j-coins[i-1]];
			}
		}
	}
	cout << dp[size][N];
    return 0;
}
