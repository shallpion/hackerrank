#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main(){
    string time;
    cin >> time;
    if(time[8] == 'A') {
        if(time.substr(0, 2) != "12")
            cout << time.substr(0, 8);
        else 
            cout << "00" + time.substr(2,6);
    }
    else {
        int h = stoi(time.substr(0,2));
        h %= 12;  h += 12;
        if(h < 10) 
            cout << "0" + to_string(h) + time.substr(2, 6);
        else
            cout << to_string(h) + time.substr(2, 6);
    }
    return 0;
}
