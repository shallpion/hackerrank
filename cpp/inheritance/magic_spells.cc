void counterspell(Spell *spell) {
	if(Fireball *frb = dynamic_cast<Fireball*>(spell)) {
		frb->revealFirepower();
	} else if(Frostbite *ftb = dynamic_cast<Frostbite*>(spell)) {
		ftb->revealFrostpower();
	} else if(Thunderstorm *ts = dynamic_cast<Thunderstorm*>(spell)) {
		ts->revealThunderpower();
	} else if(Waterbolt *wb = dynamic_cast<Waterbolt*>(spell)) {
		wb->revealWaterpower();
	} else {
		//generic spell
		string A = SpellJournal::read();
		string B = spell->revealScrollName();
		int m = A.size();   int n = B.size();
		// we use a dp thing to solve it
		vector<vector<int>> dp(m+1, vector<int>(n+1,0));
		for(int i = 1; i <= m; i++) {
			for(int j = 1; j <= n; j++){
				if(A[i-1] == B[j-1]) {
					dp[i][j] = 1 + dp[i-1][j-1];
				} else {
					dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
				}
			}
		}
		cout << dp[m][n] << endl;

	}
}
