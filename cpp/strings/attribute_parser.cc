
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
using namespace std;

class Tag {
public:
	string tagname;
	unordered_map<string, string> attrs;
	unordered_map<string, Tag *>child;
	Tag(const string &t) : tagname(t) {}
};

void tokenize(vector<string> &tkns, const string &s) {
	unsigned i = 0;
	while(i < s.size()) {
		if(isspace(s[i])) {
			i++;	continue;
		}
		unsigned j = 0;
		switch(s[i]) {
			case '<':
			case '.':	// for query
			case '~':	// for query
			case '>':
			case '=':
			case '/':
				tkns.push_back(string(1, s[i]));
				i++;
				break;
			case '"':
				j = ++i;
				while(j < s.size() && s[j] != '"') ++j;
				tkns.push_back(s.substr(i, j-i));
				i=j+1;
				break;
			default:
				j = i;
				while(j < s.size() && (isalpha(s[j]) || isdigit(s[j]))){
					j++;
				}
				tkns.push_back(s.substr(i, j-i));
				i = j;
		}
	}
}

void parse_attr(unordered_map<string, string> &attrs, const vector<string> &tkns, int &i) {
	while(tkns[i] != ">") {
		attrs[tkns[i]] = tkns[i+2];
		i += 3;
	}
}

unordered_map<string, Tag *> parse_scope(const vector<string> &, const string &, int &);

Tag* parse_expr(const vector<string> &tkns, int &i)
{
	if(tkns[i++] != "<") {
		cout << "missing: " << tkns[i-1] << endl;
		abort();
	}

	Tag *t;
	if(tkns[i] != "/") {
		// tagname
		t = new Tag(tkns[i++]);

		if(tkns[i] != ">") {
			// attribute
			parse_attr(t->attrs, tkns, i);
		} 
		t->child = parse_scope(tkns, t->tagname, ++i);

	} else {
		// closeing tag
		i+=3;
		return nullptr;
	}
	return t;
}

unordered_map<string, Tag*> 
parse_scope(const vector<string> &tkns, const string &stop, int &i) {
	cout << "woof"<< endl;
	unordered_map<string, Tag*> scope;
	while(i+2 < (int)tkns.size() && (tkns[i+1] != "/" || tkns[i+2] != stop)) {
		Tag *t = parse_expr(tkns, i);
		scope[t->tagname] = t;
	}
	i += 4;
	return scope;
}

void query(vector<string> &q, unordered_map<string, Tag *> &gs){
	unsigned i = 0;
	Tag *t;
	if(gs.count(q[0]) == 0){
		cout <<"Not Found!" << endl;
		return;
	} else {
		t = gs[q[0]];
	}
	i++;
	while(i < q.size()) {
		if(q[i] == ".") {
			i++;
			if(t->child.count(q[i]) == 0) {
				cout << "Not Found!" << endl;
				return;
			} else {
				t = t->child[q[i]];
			}
		} else if (q[i] == "~") {
			i++;
			if(t->attrs.count(q[i]) == 0) {
				cout << "Not Found!" << endl;
				return;
			} else {
				cout << t->attrs[q[i]]<< endl;
			}
		} 
		i++;
	}

}
int main() {
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */   
	unordered_map<string, Tag*> tags;
	int N, Q;
	cin >> N >> Q;
	vector<string> tkns;
	string s;
	cin.ignore();
	for(int i = 0; i < N; i++) {
		getline(cin, s);
		tokenize(tkns, s);
	}
	// marker
	tkns.push_back("<");
	tkns.push_back("/");
	tkns.push_back("###");
	int i = 0;
	unordered_map<string, Tag *> global_scope = parse_scope(tkns, "###", i);

	// queries
	vector<string> q;
	for(int i = 0; i < Q; i++) {
		q.clear();
		getline(cin, s);
		tokenize(q, s);
		query(q, global_scope);
	}



	return 0;
}

