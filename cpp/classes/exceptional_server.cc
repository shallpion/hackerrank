		/* Enter your code here. */

        try {
            int res = Server::compute(A, B);
            cout << res << endl;
        }  catch (std::bad_alloc& ba) {
            cout << "Not enough memory" << endl; 
        }  catch (std::exception& a) {
            cout << "Exception: " << a.what() << endl;
        } catch (...) {
            cout << "Other Exception" << endl;
        }

