#include<iostream>
using namespace std;
// greedy with a customized quicksort
struct Workshop {
	int start_time;
	int duration;
	int end_time;
	Workshop() :start_time(0), duration(0), end_time() {}
	Workshop(int st, int d) : start_time(st), duration(d), end_time(st+d) {}
};

struct Available_Workshops {
	int n;
	Workshop *ws;
};
void swap(int &a, int &b) {
	int tmp = a;
	a = b;
	b = tmp;
}
void sortws(int a[], int b[], int l, int r) {
	if(l >= r) return;
	int i = l, pivot = l;
	for(; i < r; i++) {
		if(a[i]+b[i] < a[r]+b[r] || (a[i]+b[i] == a[r]+b[r] && a[i] < a[r])){
			swap(a[i], a[pivot]);
			swap(b[i], b[pivot]);
			pivot++;
		}
	}
	swap(a[pivot], a[r]);
	swap(b[pivot], b[r]);
	sortws(a, b, l, pivot-1);
	sortws(a, b, pivot+1, r);

}

Available_Workshops *initialize(int start_time[], int duration[], int n) {
	Workshop *ws = new Workshop[n];
	//
	sortws(start_time, duration, 0, n-1);
	for(int i = 0; i < n; i++) {
		ws[i] = Workshop(start_time[i], duration[i]);
	}
	Available_Workshops *aw = new Available_Workshops;
	aw->n = n;
	aw->ws = ws;
	return aw;
}


int CalculateMaxWorkshops(Available_Workshops *aw){
	int ret = 0;
	int end = aw->ws[0].start_time;
	for(int i = 0; i < aw->n; i++) {
		if((aw->ws)[i].start_time >= end) {
			end = (aw->ws)[i].end_time;
			ret++;
		}
	}
	return ret;
}

int main() {
	int a[6] = {1,3, 0, 5, 5, 8};
	int b[6] = {1, 1, 6, 2, 4, 1};
	cout << CalculateMaxWorkshops(initialize(a, b, 6));
}

