// why is this hard?
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */ 
    const unsigned m = 1u<<31;
    int unique = 1;
    unsigned long N, S, P, Q;
    cin >> N >> S >> P >> Q;

    vector<bool> a(m, false);
    a[S] = true;
    for(unsigned long i = 1; i < N; i++) {
        S = (S*P + Q) % m;
        if(!a[S]){
            a[S] = true;
            unique++;
        }
    }
    cout << unique << endl;
    return 0;
}
