// metaprogramming

template<bool...> struct rbv;

template<bool b1, bool ...rest>
struct rbv<b1, rest...>
{
    static const int value = b1 + 2*rbv<rest...>::value;
    static int output() {       
		return value;
    }
};

template<>
struct rbv<>
{
    static const size_t value = 0;
};

template<bool...b>
int reversed_binary_value() {
	return rbv<b...>::output();
}

