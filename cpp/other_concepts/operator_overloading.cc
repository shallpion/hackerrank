class Matrix {
public:
    vector<vector<int>> a;
    //overload assignment at first
    Matrix &operator=(const Matrix &b) {
        for(const vector<int> &v : b.a) {
            a.push_back(v);
        }
        return *this;
    }
    vector<int> &operator[](int i) {
        return a[i];
    }
    Matrix operator+(Matrix &b) {
        Matrix c = *this;  
        for(unsigned i = 0; i < a.size(); i++) {
            for(unsigned j = 0; j < a[0].size(); j++) {
                c[i][j] += b[i][j];
            }
        }
        return c;
    }
};
