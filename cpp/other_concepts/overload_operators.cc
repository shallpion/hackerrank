const Complex operator+(const Complex &lhs, const Complex &rhs) {
    Complex c;
    c.a = lhs.a + rhs.a;
    c.b = lhs.b + rhs.b;
    return c;
}
ostream &operator<<(ostream &os, const Complex &rhs) {
    return os<<rhs.a << "+i" << rhs.b;
}
