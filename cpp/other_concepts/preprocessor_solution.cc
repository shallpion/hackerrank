
#define toStr(s) #s    //stringing operator
#define io(v) cin >> (v)
#define FUNCTION(f, sign) void (f) (int &a, int b) { a = (a sign b ? a : b); }
#define INF 0x5fffffff
#define foreach(v, i) for(int (i) = 0; (i) < (int) (v).size(); ++(i))
