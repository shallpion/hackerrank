#include <iostream>
#include <deque> 
using namespace std;
void printKMax(int arr[], int n, int k){
	// We use a sliding deque to record index, instead of numbers. So it is
	// easier to record whether we need to popup. Notice that the numbers (not indices) 
	// in the deque is decreasing, with the front being always the designed max
	deque<int> dq;
	for(int i = 0; i < n; i++) {
		if(!dq.empty() && dq.front() == i - k) {
			// front has to be pushed out
			dq.pop_front();
		} 
		while(!dq.empty() && arr[dq.back()] < arr[i]) {
			dq.pop_back();
		}
		dq.push_back(i);
		if(i >= k-1) cout << arr[dq.front()] << " ";

	}
	cout << endl;

}
int main(){

	int t;
	cin >> t;
	while(t>0) {
		int n,k;
		cin >> n >> k;
		int i;
		int arr[n];
		for(i=0;i<n;i++)
			cin >> arr[i];
		printKMax(arr, n, k);
		t--;
	}
	return 0;
}
