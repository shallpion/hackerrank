#include <iostream>
#include <iomanip> 
using namespace std;

int main() {
	int T = 1;
	cout << setiosflags(ios::uppercase);
	cout << setw(0xf) << internal;
	while(T--) {
		double A = 9.859;
		double B = 748.096;
		double C = 475.634;

		cout << nouppercase;
		cout << setw(0) << left;
		cout << std::hex << std::showbase << (long long) A << std::dec << endl;

		cout << setw(0xf) << right;
		cout << setfill('_');
		cout << showpos;
		cout << fixed << setprecision(2);
		cout << B << endl;

		cout << left;
		cout << noshowpos;
		cout << uppercase;
		cout << scientific;
		cout << setprecision(9);
		cout << C << endl;
	}
}
